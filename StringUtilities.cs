﻿using System;
using System.Collections.Generic;
using System.Text;
#region String Utilites

public static class StringUtilities {

  public static string Join(this IEnumerable<string> strings, string delimiter) {
    return string.Join(delimiter, strings);
  } 

  /// <summary>
  /// check contains using special string comparison
  /// </summary>
  public static bool Contains(this string sourceString, string substring, StringComparison stringComparison) {
    return sourceString?.IndexOf(substring, stringComparison) >= 0;
  }

  /// <summary>
  /// Add a space to a sentince where caps seperate each char
  /// </summary>
  public static string AddSpacesToCappedSentence(this string text, bool preserveAcronyms = true) {
    if (string.IsNullOrWhiteSpace(text))
      return string.Empty;
    StringBuilder newText = new StringBuilder(text.Length * 2);
    newText.Append(text[0]);
    for (int i = 1; i < text.Length; i++) {
      if (char.IsUpper(text[i]))
        if ((text[i - 1] != ' ' && !char.IsUpper(text[i - 1])) ||
            (preserveAcronyms && char.IsUpper(text[i - 1]) &&
             i < text.Length - 1 && !char.IsUpper(text[i + 1])))
          newText.Append(' ');
      newText.Append(text[i]);
    }
    return newText.ToString();
  }

  /// <summary>
  /// Get an int from a base64 string
  /// </summary>
  public static int ToIntFromBase64(this string base64string) {
    var bytes = Convert.FromBase64String(base64string);
    return BitConverter.ToInt32(bytes, 0);
  }

  /// <summary>
  /// extra split
  /// </summary>
  public static IEnumerable<string> Split2(this string source, string delim) {
    // argument null checking etc omitted for brevity

    int oldIndex = 0, newIndex;
    while ((newIndex = source.IndexOf(delim, oldIndex)) != -1) {
      yield return source.Substring(oldIndex, newIndex - oldIndex);
      oldIndex = newIndex + delim.Length;
    }
    yield return source.Substring(oldIndex);
  }
}

#endregion