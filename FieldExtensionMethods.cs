﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using PropertyAccess;

public static class FieldExtensionMethods {

  /// <summary>
  /// Cache the getters for properties to speed them up
  /// </summary>
  static Dictionary<string, Delegate> getterCache
    = new Dictionary<string, Delegate>();

  static Dictionary<string, IPropertyReadAccess> dynamicGetterCache
    = new Dictionary<string, IPropertyReadAccess>();

  public static object GetValue(this MemberInfo memberInfo, object forObject) {
    switch (memberInfo.MemberType) {
      case MemberTypes.Field:
        return ((FieldInfo)memberInfo).GetValue(forObject);
      case MemberTypes.Property:
        // if it's cached
        /*if (getterCache.TryGetValue(forObject.GetType() + "::" + memberInfo.Name, out Delegate getter)) {
          return ((dynamic)getter)();
        }*/
        Type objectType = forObject.GetType();

        // build the key efficiently:
        StringBuilder methodKeyBuilder = new StringBuilder(objectType.FullName);
        methodKeyBuilder.Append("::");
        methodKeyBuilder.Append(memberInfo.Name);
        string methodKey = methodKeyBuilder.ToString();

        // check if it's cached:
        if (dynamicGetterCache.TryGetValue(methodKey, out IPropertyReadAccess propertyAccess)) {
          return propertyAccess.GetValue(forObject);
        }

        // Build a property accessor if it's not:
        IPropertyReadAccess propertyAccessor
          = dynamicGetterCache[methodKey]
          = memberInfo.DeclaringType.IsValueType
             ? (IPropertyReadAccess)PropertyAccessFactory.CreateForValue(objectType, memberInfo.Name)
             : PropertyAccessFactory.CreateForClass(objectType, memberInfo.Name);

        // if it's not:
        /*Type getterReturnType = memberInfo.DataType();
        Type getterDelegateType = typeof(Func<>).MakeGenericType(getterReturnType);

        getter = getterCache[forObject.GetType() + "::" + memberInfo.Name]
          = Delegate.CreateDelegate(
              getterDelegateType,
              forObject,
              ((PropertyInfo)memberInfo).GetGetMethod(true)
            );

        return ((dynamic)getter)();*/
        return propertyAccessor.GetValue(forObject);
      default:
        throw new NotImplementedException();
    }
  }

  public static Type DataType(this MemberInfo memberInfo) {
    switch (memberInfo.MemberType) {
      case MemberTypes.Field:
        return ((FieldInfo)memberInfo).FieldType;
      case MemberTypes.Property:
        return ((PropertyInfo)memberInfo).PropertyType;
      default:
        throw new NotImplementedException();
    }
  }
}