﻿using Newtonsoft.Json.Linq;

/// <summary>
/// TODO move json newtonsoft to a new library Meep.Tech.Utilities.Json
/// </summary>
public static class JsonnetExtensions {

  /// <summary>
  /// Try and get a value and just return it or null
  /// </summary>
  /// <typeparam name="T"></typeparam>
  /// <param name="jObject"></param>
  /// <param name="propertyName"></param>
  /// <returns></returns>
  public static T TryGetValue<T>(this JObject jObject, string propertyName) {
    return jObject.TryGetValue(propertyName, out JToken value)
      ? value.Value<T>()
      : default;
  }
}
