﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;
#region Type Utilities

public static class TypeExtensions {

  #region Cast Functionality

  static readonly Dictionary<Tuple<Type, Type>, Func<object, object>> CastCache
    = new Dictionary<Tuple<Type, Type>, Func<object, object>>();

  static Func<object, object> MakeCastDelegate(Type originalType, Type resultingType) {
    var inputObject = Expression.Parameter(typeof(object));
    return Expression.Lambda<Func<object, object>>(
        Expression.Convert(
          Expression.ConvertChecked(
            Expression.Convert(inputObject, originalType),
            resultingType
          ),
          typeof(object)
        ),
        inputObject
      ).Compile();
  }

  static Func<object, object> GetCastDelegate(Type from, Type to) {
    lock (CastCache) {
      var key = new Tuple<Type, Type>(from, to);
      Func<object, object> cast_delegate;
      if (!CastCache.TryGetValue(key, out cast_delegate)) {
        try {
          cast_delegate = MakeCastDelegate(from, to);
        } catch (Exception e) {
          throw new NotImplementedException($"No cast found from:\n\t{from.FullName},\nto:\n\t{to.FullName}.\n\n{e}");
        }
        CastCache.Add(key, cast_delegate);
      }
      return cast_delegate;
    }
  }

  /// <summary>
  /// Tries to cast an object to a given type. First time is expensive
  /// </summary>
  public static object CastTo(this object @object, Type type) {
    return GetCastDelegate(@object.GetType(), type).Invoke(@object);
  }

  #endregion

  /// <summary>
  /// Get the generic arguments from a type this inherits from
  /// </summary>
  /// <returns></returns>
  public static IEnumerable<Type> GetInheritedGenericTypes(this Type type, Type genericParentType) {
    List<Type> inheritedGenericTypes = new List<Type>();
    foreach (Type intType in type.GetParentTypes()) {
      if (intType.IsGenericType && intType.GetGenericTypeDefinition() == genericParentType) {
        return intType.GetGenericArguments();
      }
    }

    return inheritedGenericTypes;
  }

  /// <summary>
  /// Get all parent types and interfaces 
  /// </summary>
  /// <param name="type"></param>
  /// <returns></returns>
  public static IEnumerable<Type> GetParentTypes(this Type type) {
    // is there any base type?
    if (type == null) {
      yield break;
    }

    // return all implemented or inherited interfaces
    foreach (var i in type.GetInterfaces()) {
      yield return i;
    }

    // return all inherited types
    var currentBaseType = type.BaseType;
    while (currentBaseType != null) {
      yield return currentBaseType;
      currentBaseType = currentBaseType.BaseType;
    }
  }

  public static bool IsAutoProperty(this PropertyInfo property) {
    string backingFieldName = $"<{property.Name}>k__BackingField";
    var backingField = property.DeclaringType.GetField(backingFieldName, BindingFlags.NonPublic | BindingFlags.Instance);

    return backingField != null && backingField.GetCustomAttribute(typeof(CompilerGeneratedAttribute)) != null;
  }

  public static bool IsNullable(this Type type) {
    return Nullable.GetUnderlyingType(type) != null;
  }

  public static bool IsAssignableToGeneric(this Type givenType, Type genericType) {
    var interfaceTypes = givenType.GetInterfaces();

    foreach (var it in interfaceTypes) {
      if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
        return true;
    }

    if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
      return true;

    Type baseType = givenType.BaseType;
    if (baseType == null) return false;

    return baseType.IsAssignableToGeneric(genericType);
  }
  
  /// <summary>
  /// Gets the first class, earliest in the inheretence tree, thats not just 'object'
  /// </summary>
  /// <param name="type"></param>
  /// <returns></returns>
  public static Type GetMostBasicUniqueClass(this Type type) {
    Type currentType = type;
    while (currentType.BaseType != typeof(object)) {
      currentType = currentType.BaseType;
    }

    return currentType;
  }
}

#endregion