﻿public static class RangeUtilities {

  /// <summary>
  /// Scale a float value to a new set of maxes and mins.
  /// </summary>
  /// <param name="value"></param>
  /// <param name="newMax"></param>
  /// <param name="newMin"></param>
  /// <param name="oldMax"></param>
  /// <param name="oldMin"></param>
  /// <returns></returns>
  public static float scale(this float value, float newMax, float newMin, float oldMax = 1.0f, float oldMin = -1.0f) {
    float scaled = newMin + (value - oldMin) / (oldMax - oldMin) * (newMax - newMin);
    return scaled;
  }

  /// <summary>
  /// fast clamp a float to between 0 and 1
  /// </summary>
  /// <param name="value"></param>
  /// <param name="minValue"></param>
  /// <param name="maxValue"></param>
  /// <returns></returns>
  public static float clampToFloat(float value, int minValue, int maxValue) {
    return (
      (value - minValue)
      / (maxValue - minValue)
    );
  }

  /// <summary>
  /// fast clamp float to short
  /// </summary>
  /// <param name="value"></param>
  /// <param name="minFloat"></param>
  /// <param name="maxFloat"></param>
  /// <returns></returns>
  public static short clampToShort(float value, float minFloat = 0.0f, float maxFloat = 1.0f) {
    return (short)((short.MaxValue - short.MinValue)
      * ((value - minFloat) / (maxFloat - minFloat))
      + short.MinValue);
  }

  /// <summary>
  /// Clamp a value between two numbers
  /// </summary>
  /// <param name="value"></param>
  /// <param name="startingMin"></param>
  /// <param name="startingMax"></param>
  /// <param name="targetMin"></param>
  /// <param name="targetMax"></param>
  /// <returns></returns>
  public static double clamp(double value, double startingMin, double startingMax, double targetMin, double targetMax) {
    return (targetMax - targetMin)
      * ((value - startingMin) / (startingMax - startingMin))
      + targetMin;
  }

  /// <summary>
  /// Clamp the values between these numbers in a non scaling way.
  /// </summary>
  /// <param name="number"></param>
  /// <param name="min"></param>
  /// <param name="max"></param>
  /// <returns></returns>
  public static float box(this float number, float min, float max) {
    if (number < min)
      return min;
    else if (number > max)
      return max;
    else
      return number;
  }

  /// <summary>
  /// Box a float between 0 and 1
  /// </summary>
  /// <param name="number"></param>
  /// <returns></returns>
  public static float box01(this float number) {
    return box(number, 0, 1);
  }
}